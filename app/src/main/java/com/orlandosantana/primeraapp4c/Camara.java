package com.orlandosantana.primeraapp4c;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Camara extends Activity implements View.OnClickListener{
    Button buttonCamara, buttonGuardar;
    ImageView imageViewFoto;
    Bitmap imageBitmap;
    EditText editTextNombre;
    private  final int REQUEST_PERMISSION_SAVE = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);

        buttonCamara =(Button) findViewById(R.id.buttonCamara);
        buttonGuardar =(Button) findViewById(R.id.buttonGuardar);
        imageViewFoto = (ImageView)findViewById(R.id.imageViewFoto);
        editTextNombre = (EditText) findViewById(R.id.editTextNombre);
        buttonCamara.setOnClickListener(this);

        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarImagen();
            }
        });
    }
    static final int REQUEST_IMAGE = 1;
    @Override
    public void onClick(View v) {
        Intent abrirCamara = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(abrirCamara.resolveActivity(getPackageManager())!=null){
            startActivityForResult(abrirCamara,REQUEST_IMAGE);
        }
    }
    private void GuardarFoto(String direccion,
                             String nombreFoto,
                             Bitmap bitman){
        try {

            File foto = new File(direccion, nombreFoto );
            FileOutputStream stream = new FileOutputStream(foto);
            bitman.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void GuardarImagen(){
        if ((!tienePermisoSDCard())){
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_SAVE);
        }else{
            if (imageViewFoto.getDrawable() != null){
                CrearCarpeta();
                String dirrecion =
                        Environment.getExternalStorageDirectory().getAbsolutePath() + "/ImagenesCameraS/";
                GuardarFoto(dirrecion, editTextNombre.getText()+".JPEG", imageBitmap);
                Toast.makeText(getApplicationContext(), "Imagen Guardada exitosamente", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void CrearCarpeta(){
        String nombreCarpeta = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+"Imagenes4C";
        File carpeta = new File(nombreCarpeta);
        if(!carpeta.exists()){
            if(!carpeta.mkdir()){
                Toast.makeText(this, "Carpeta no fue creada", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "Carpeta creada exitosamente", Toast.LENGTH_LONG).show();
            }
        }
    }
    //Pedir permiso
    private boolean tienePermisoSDCard(){
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode ==  REQUEST_IMAGE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            //Asignar al objeto de tipo bitmap lo que me devuelve el objeto data.
            imageBitmap = (Bitmap) extras.get("data");
            imageViewFoto.setImageBitmap(imageBitmap);

        }
    }
    }

